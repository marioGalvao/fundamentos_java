package br.com.devmedia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.devmedia.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
