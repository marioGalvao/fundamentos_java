package br.com.devmedia.api;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.devmedia.domain.Produto;
import br.com.devmedia.exception.ProdutoNotFoundException;
import br.com.devmedia.repository.ProdutoRepository;

@RestController
@RequestMapping("/api/v1.0")
public class ProdutoApi {
	@Autowired
	ProdutoRepository produtoRepository;

	@GetMapping("/produto")
	public List<Produto> getAllProduto() {
		return produtoRepository.findAll();
	}

	@GetMapping("/produto/{id}")
	public Produto getProdutoById(@PathVariable(value = "id") Long id) {
		return produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNotFoundException("Produto", "id", id));
	}

	@PostMapping("/produto")
	public Produto createProduto(@Valid @RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}

	@PutMapping("/produto/{id}")
	public Produto updateProduto(@PathVariable(value = "id") Long id, @Valid @RequestBody Produto produtoDetails) {

		Produto produto = produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNotFoundException("Produto", "id", id));

		produto.setNome(produtoDetails.getNome());
		produto.setDescricao(produtoDetails.getDescricao());
		produto.setEstoque(produtoDetails.getEstoque());
		produto.setValorProduto(produtoDetails.getValorProduto());

		Produto updatedProduto = produtoRepository.save(produto);
		return updatedProduto;
	}

	@DeleteMapping("/produto/{id}")
	public ResponseEntity<Produto> deleteProduto(@PathVariable(value = "id") Long id) {

		Produto produto = produtoRepository.findById(id)
				.orElseThrow(() -> new ProdutoNotFoundException("Produto", "id", id));

		produtoRepository.delete(produto);
		return ResponseEntity.ok().build();
	}
}
